require 'bigdecimal/math'
require 'prime'

pi = BigMath::PI(10000).to_s

# 0.のところは飛ばす
offset = 2

while true
  digits = pi[offset, 10]
  if Prime.instance.prime? digits.to_i
    puts digits + ' is Prime!!!!! '
    puts "Flag is FLAG_Q20_" + digits
    exit
  else
    puts digits + ' is not prime....'
  end
  offset += 1
end
